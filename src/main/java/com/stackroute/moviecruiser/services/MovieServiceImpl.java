package com.stackroute.moviecruiser.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.moviecruiser.domain.Movie;
import com.stackroute.moviecruiser.exceptions.MovieNotFoundException;
import com.stackroute.moviecruiser.repositories.MovieDAO;

@Service
@Transactional
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieDAO movieDAO;

	/**
	 * Save the movie.
	 * 
	 */
	public boolean saveMovie(Movie movie) throws MovieNotFoundException {
		movieDAO.saveMovie(movie);
		return true;
	}

	/**
	 * update the movie.
	 */
	public Movie updateMovieComments(Integer id, String comments) throws MovieNotFoundException {
		return movieDAO.updateMovieComments(id, comments);

	}

	/**
	 * Delete the movie.
	 */
	public boolean deleteMovieById(int id) throws MovieNotFoundException {
		return movieDAO.deleteMovieById(id);
	}

	/**
	 * Return the movie having the passed id.
	 */
	public Movie getMovieById(int id) throws MovieNotFoundException {
		return movieDAO.getMovieById(id);
	}
}
